import axios from 'axios'

const url = require('url')
const metascraper = require('metascraper')([
  require('metascraper-description')(),
  require('metascraper-image')(),
  require('metascraper-title')()
])

export default async function(req, res, next) {
  let queryData = url.parse(req.url, true).query

  const content = await axios.get(queryData.target)
  const result = await metascraper({ html: content.data, url: queryData.target })
  await res.end(JSON.stringify(result))
}
